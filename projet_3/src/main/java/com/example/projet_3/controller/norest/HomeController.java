package com.example.projet_3.controller.norest;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @Value("${home.message}")
    private String homeMessage;

    @GetMapping("/")
    public String home(){
        return homeMessage;
    }

    @GetMapping("/login")
    public String login(){
        return "login.html";
    }

    @PostMapping("/login")
    public void loginSave(){
//        return "login.html";
        System.out.println("login post");
    }
}
