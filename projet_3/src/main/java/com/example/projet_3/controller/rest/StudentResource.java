package com.example.projet_3.controller.rest;


import com.example.projet_3.dao.StudentDao;
import com.example.projet_3.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/")
public class StudentResource {

    @Autowired
    private StudentDao studentDao;

    // get all

    @GetMapping("students")
    public List<Student> getAll(){
       return studentDao.findAll();
    }
    // get by id

    // add

    @PostMapping("students")
    public ResponseEntity<Student> createStudent(@RequestBody Student st){
        Student savedstudent = studentDao.save(st);
        return ResponseEntity.status(200).build();
    }

    // update

    //delete
}
