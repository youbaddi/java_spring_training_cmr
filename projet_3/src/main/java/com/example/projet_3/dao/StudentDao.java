package com.example.projet_3.dao;

import com.example.projet_3.model.Student;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;


import java.util.List;


@RepositoryRestResource
public interface StudentDao extends JpaRepository<Student,Long> {

    List<Student> findByNom( @Param("nom") String nom);

    @RestResource(path="nomContains")
    List<Student> findByNomContains(@Param("nom") String nom, Pageable pageable);


    @Query()
    List<Student> findByNomContains(@Param("nom") String nom, Pageable pageable);
}
