public class Professeur extends Personne {
    private int numSomme;



    Professeur(int num, String nom, String prenom,int ns ){
        super(num,nom,prenom);
        numSomme=numSomme;
    }

    public int getNumSomme() {
        return numSomme;
    }

    public void setNumSomme(int numSomme) {
        this.numSomme = numSomme;
    }

    @Override
    public String toString() {
        return "Professeur{" +
                "num=" + getNum() +
                ", nom='" + getNom() + '\'' +
                ", prenom='" + getPrenom() + '\'' +
                ", numSomme=" + numSomme +
                '}';
    }

    public String  calculeNomComplet(){
        return "Professeur " + getNom() +" " + getPrenom();
    }
}
