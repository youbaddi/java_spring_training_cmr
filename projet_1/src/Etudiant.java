public class Etudiant extends Personne implements CalculeNomComplet{
    private int cne;


    Etudiant(int num, String nom, String prenom,int cne ){
        super(num,nom,prenom);
        this.cne=cne;
    }

    public int getCne() {
        return cne;
    }

    public void setCne(int cne) {
        this.cne = cne;
    }

    @Override
    public String toString() {
        return "Etudiant{" +
                "num=" + getNum() +
                ", nom='" + getNom() + '\'' +
                ", prenom='" + getPrenom() + '\'' +
                ", cne=" + cne +
                '}';
    }

//    @Override
//    public String calculeNomComplet() {
//        return "Etudiant " + getNom() +" " + getPrenom();
//    }
}
