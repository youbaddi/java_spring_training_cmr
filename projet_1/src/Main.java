import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        Personne p1 = new Personne(10, "baddi", "youssef", true);

        System.out.println(p1.getNom());

        System.out.println(p1.hashCode());
        System.out.println(p1);
        System.out.println(p1.calculeNomComplet());
        System.out.println(p1.im);
        System.out.println(p1.im2);
        p1.im.afficherDetails("h");
        p1.im2.afficherDetails("h");
        p1.im2.afficherDetails2();

        System.out.println(p1.im2.getClass().getSuperclass());

        Professeur pr1 = new Professeur(1, "baddi", "youssef", 13000);
        Professeur pr2 = new Professeur(2, "baddi", "youssef", 13000);
        Personne pr3 = new Professeur(3, "baddi", "youssef", 13000);

        System.out.println(pr1.getNom());

        System.out.println(pr1.toString());
        System.out.println(pr2.toString());
        System.out.println(pr1.hashCode());
        System.out.println(pr1.calculeNomComplet());

        Etudiant e1= new Etudiant(1, "baddi", "youssef", 123456);



        List<Personne> personnes = new ArrayList<Personne>();
        personnes.add(p1);
        personnes.add(pr1);
        personnes.add(pr2);
        personnes.add(pr3);
        personnes.add(e1);

        for(Personne p : personnes){
            System.out.println(p.calculeNomComplet());
        }

        System.out.println("==================");
        personnes.forEach(System.out::println);
        System.out.println("==================");

        personnes.removeIf(personne -> personne.getNum()==3);

        System.out.println("==================");
        personnes.forEach(System.out::println);
        System.out.println("==================");
        int i=2, j=3;
        ICalcul plus = (x, y) -> x+y;
        ICalcul moins = (x, y) -> x-y;

        ICalcul multi = (x, y) -> x*y;

        ICalcul devi = (x, y) -> {
            int tmp = i;
            tmp++;
            if(y==0) return tmp+j;
            else return x/y;
        };


        System.out.println(plus.operationBinaire(5,3));
        System.out.println(moins.operationBinaire(5,3));

        System.out.println(multi.operationBinaire(5,3));
        System.out.println(devi.operationBinaire(5,0));

        Function<Integer, String> f1 = x -> {return x+"!";};

        System.out.println(f1.apply(3));


    }
}
