import java.util.Objects;

public class Personne implements CalculeNomComplet {
    private int num;
    private String nom;
    private String prenom;



    public IMiseEnForme im = new IMiseEnForme() {
        @Override
        public String afficherDetails(String n) {
            return n; //System.out.println("instance 1");
        }
    };

    public IMiseEnForme im2 = (n) ->  "instance 2";

    Personne(){
        super();
        this.nom="";
        this.prenom="";

    }

    Personne(String nom, String prenom){
        this.nom=nom;
        this.prenom=prenom;

    }

    Personne(int num, String nom, String prenom){
        this.num=num;
        this.nom=nom;
        this.prenom=prenom;

    }
    Personne(int num, String nom, String prenom, boolean t){
        this.num=num;
            if(t){
                this.nom=prenom;
                this.prenom=nom;
            }else{
                this.nom=nom;
                this.prenom=prenom;
            }
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    void afficherDetails(){
        System.out.println(this.nom);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Personne personne = (Personne) o;
        return num == personne.num && Objects.equals(nom, personne.nom) && Objects.equals(prenom, personne.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(num, nom, prenom);
    }

    @Override
    public String toString() {
        return "Personne{" +
                "num=" + num +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                '}';
    }


    @Override
    public String calculeNomComplet() {
        return "Personne " + getNom() +" " + getPrenom();
    }
}
