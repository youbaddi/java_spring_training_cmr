import java.time.Month;
import java.time.Year;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {


    public static void main(String[] args) {


        List list = new ArrayList<>(Arrays.asList(3,6,8,9,1,10));

        Stream<Integer> stream1 = list.stream();

        stream1.map(elt->elt+2).forEach(elt -> System.out.println(elt));


        System.out.println("==============");
        Stream stream2 = list.stream();
        stream2.forEach(System.out::println);

        System.out.println("==============");
        List<String> marques = Arrays.asList("peugeot", "ford", "mercedes", " cooper");
        Stream<String> stream3 = marques.stream();
//        stream3.forEach(elt -> System.out.println(elt.length()));
//        stream3.map(elt->elt.length()).forEach(System.out::println);
        stream3.
                map(elt->elt.length()).
                filter(elt->elt%2==0).
                forEach(System.out::println);

        System.out.println("==============");
        for(String str : marques){
            int length = str.length();
            if(length % 2 == 0){
                System.out.println(length);
            }
        }

        System.out.println("==============");
        List listEmpty = new ArrayList<>(Arrays.asList());

        Stream<Integer> stream4 = listEmpty.stream();

        Optional<Integer> op =stream4.
                map(elt->elt+2).
                reduce((a,b)->a+b);

        if(op.isPresent()){
        System.out.println(op.get());}
        else{
            System.out.println(op.orElse(null));
            System.out.println("op is not present");
        }

        System.out.println("==============");
        Stream<Integer> stream5 = list.stream();

        stream5.
                map(elt->elt+2).
                reduce((a,b)->a+b).
                ifPresent(System.out::println);


        System.out.println("==============");
        Stream<Integer> stream6 = list.stream();

        int sommeItems = stream6.
                map(elt->elt+2).
                reduce((a,b)->a+b).orElse(0);
        System.out.println(sommeItems);

        System.out.println("==============");
        Stream stream7 = list.stream();
        stream7.forEach(System.out::println);

        System.out.println("==============");
        Stream<Integer> stream8 = list.stream();
        list = stream8.map(elt->elt+2).
                collect(Collectors.toList())
                ;

        System.out.println("==============");
        Stream stream9 = list.stream();
        stream9.forEach(System.out::println);

        System.out.println("==============");
        List<String> marques2 = Arrays.asList("peugeot","pentli", "ford", "ferari", "mercedes", "cooper", "cadiac");
        Stream<String> stream10 = marques2.stream();

        stream10.collect(Collectors.groupingBy(elt->((String)elt).charAt(0))).
                forEach((k,v)->System.out.println(k+ " " + v));



        // Date

        Date d = new Date();
        System.out.println(d);
        System.out.println(d.getYear());

        Year y = Year.now();
        System.out.println(y.getValue());

        Year y2 = Year.of(2024);
        System.out.println(y2.getValue());

        System.out.println(y2.isLeap());


        System.out.println(y.plusYears(3));
        System.out.println(y.minusYears(6));

        Month m = Month.of(2);
        System.out.println(m);
        System.out.println(m.getDisplayName(TextStyle.FULL, Locale.UK));

        System.out.println(m.length(Year.of(2024).isLeap()));
        System.out.println(m.maxLength());
    }
}
